def do_turn(pw):
	if len(pw.my_planets()) > 0 or len(pw.enemy_planets()) > 0:
		oa = get_optimal_attack(pw)
		hp = pw.my_planets()[0]
		if hp.num_ships() > oa.num_ships():
			pw.issue_order(hp, oa, oa.num_ships()+1)
	
def distance(a, b):
	dx = max(a.x(), b.x()) - min(a.x(), b.x())
	dy = max(a.y(), b.y()) - min(a.y(), b.y())
	return (dx*dx + dy*dy)**(0.5)

def get_optimal_attack(pw):
	p = pw.my_planets()[0]
	factors = []

	for planet in pw.enemy_planets():
		pw.debug(planet.num_ships())
		factors.append(distance(p, planet) * p.num_ships())
	pw.debug(factors)
	return pw.enemy_planets()[factors.index(min(factors))]